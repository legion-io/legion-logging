# Legion::Logging Changelog

## v1.1.1
* Updating default log level to info instead of debug
* Cleaning up some RSpec tests

## v0.1.3
* Updating `.rubocop.yml`
* Adding in logic for LOGGING-1 LOGGING-5
* Updating gemspec with more things
