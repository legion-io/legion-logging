# Legion::Logging

Legion::Logging is part of the Legion Framework


## Installation

Add this line to your application's Gemfile:

```ruby
gem 'legion-logging'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install legion-logging

## Usage

This library is utilized by Legion to log messages

## Gem

This gem can be viewed and download from [RubyGems - Legion-Logging](https://rubygems.org/gems/legion-logging)

## Contributing

Bug reports and pull requests are welcome on Bitbucket at https://bitbucket.org/legion-io/legion-logging/issues This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org) code of conduct.

## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).