lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'legion/logging/version'

Gem::Specification.new do |spec|
  spec.name          = 'legion-logging'
  spec.version       = Legion::Logging::VERSION
  spec.authors       = ['Esity']
  spec.email         = ['matthewdiverson@gmail.com']

  spec.summary       = 'The Legion::Logging Class'
  spec.description   = 'The Logger class for LegionIO'
  spec.homepage      = 'https://bitbucket.org/legion-io/legion-logging'

  spec.metadata['bug_tracker_uri'] = 'https://bitbucket.org/legion-io/legion-logging/issues?status=new&status=open'
  spec.metadata['changelog_uri'] = 'https://bitbucket.org/legion-io/legion-logging/src/CHANGELOG.md'
  spec.metadata['documentation_uri'] = 'https://bitbucket.org/legion-io/legion-logging'
  spec.metadata['homepage_uri'] = 'https://bitbucket.org/legion-io/legion-logging'
  spec.metadata['source_code_uri'] = 'https://bitbucket.org/legion-io/legion-logging'
  spec.metadata['wiki_uri'] = 'https://bitbucket.org/legion-io/legion-logging/wiki/Home'
  spec.required_ruby_version = '>= 2.5.0'
  spec.files = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.require_paths = ['lib']
  spec.add_dependency 'rainbow', '~> 3'
end
